DROP DATABASE if exists tf2_db;
CREATE DATABASE tf2_db;

<<<<<<< HEAD
=======
DROP TABLE if exists tf2_db.class;
CREATE TABLE tf2_db.class(id int PRIMARY KEY, charactername varchar(60), characterdescription text, imagesource varchar(250));

INSERT INTO tf2_db.class (id, charactername, characterdescription, imagesource)
VALUES (1, 'Scout',
 'Born and raised in Boston, Massachusetts, USA, the Scout is a fast-running scrapper with a baseball bat and a snarky in-your-face attitude.
 He is the fastest and most mobile mercenary on the battlefield unassisted. 
His Double Jump leaves slower opponents such as the Heavy struggling to keep up and helps him navigate the terrain while dodging oncoming bullets and projectiles.
Carrying a Scattergun, a Pistol, and a Bat, the Scout is an ideal class for aggressive fighting and flanking.
The Scout is a great class for quick hit-and-run tactics that can either sap away enemies health or kill them outright due to his ability to get in, do damage,
and dash away before even being noticed. However, the Scout is tied with the Engineer, Sniper, and Spy for having the lowest health of any class, leaving him vulnerable when he is on the front line;
a fair trade-off for his ability to run in and out of a contested hot-spot very quickly, letting him lead the team to victory without the other team even noticing in time.
The Scout is an excellent choice for completing objectives quickly. He can capture control points and push carts at twice the rate of any other class.
 Only the Scout has this ability naturally; the Demoman and Soldier only have the same ability the Pain Train is equipped. His speed also makes him perfect for capturing intelligence briefcases.',
 'https://static.wikia.nocookie.net/teamfortress/images/4/48/Scout_Full_Body.png/revision/latest/scale-to-width-down/340?cb=20170716194653');
 
INSERT INTO tf2_db.class (id, charactername, characterdescription, imagesource)
VALUES (2, 'Pyro',
 'The Pyro is a mumbling pyromaniac of indeterminate origin who has a burning passion for all things fire related. As shown in Meet the Pyro, the Pyro appears to be insane and delusional,
living in a utopian fantasy world known as Pyroland.
The Pyro specializes in fighting enemies at close range using a homemade Flame Thrower. Enemies set on fire suffer from afterburn and take additional damage over time,
allowing the Pyro to excel at hit-and-run tactics. Due to the Flamethrower’s short range,
the Pyro is weaker at longer ranges and relies heavily on ambushing and taking alternate routes to catch opponents off-guard.
Although categorized as an offensive class, the Pyro also brings some utility to the battlefield. The Pyro’s compression blast,
for example, can deflect enemy projectiles, extinguish burning teammates, and forcibly reposition any enemy, including one under the effects of an invincibility-granting ÜberCharge.
Because enemies hit by fire are visibly ignited, the Pyro is the best class for Spy-checking, as even a small puff of flames can nullify the effectiveness of the Spys Cloak and disguise.
Additionally, the Pyro can use the Homewrecker, the Neon Annihilator, and the Maul to protect an Engineer’s buildings from an enemy Spys Sapper.
The Pyro can also use the Detonator or Scorch Shot to Flare jump, and the Thermal Thruster jetpack to fly to places that, to the Pyro, are normally unreachable.
The Pyro wears an asbestos-lined suit that provides protection from the afterburn of other Pyros and all flame-related weaponry (except for the Dragons Fury and Gas Passer which can light even Pyros on fire),
although it does not provide any resistance to direct damage from flame throwers, flare guns or explosions. The three incendiary grenades on the Pyros character model are purely cosmetic and cannot be used.
The Pyro is voiced by Dennis Bateman.',
 'https://wiki.teamfortress.com/w/images/thumb/c/c8/Pyro.png/250px-Pyro.png');
 
INSERT INTO tf2_db.class (id, charactername, characterdescription, imagesource)
VALUES (3, 'Heavy',
 'The Heavy Weapons Guy, more commonly known as the Heavy, is a towering hulk of a man hailing from the USSR.
 He is the largest and possibly most dangerous class in Team Fortress 2. 
Boasting the most default health and devastating firepower from his trusty Minigun, the Heavy is no pushover.
The Heavys Minigun can inflict heavy damage at a high rate of fire, allowing him to mow down opposing babies, cowards, and teeny-men in seconds.
The Heavys movement speed is his main weakness. Upon revving up or firing his Minigun, his already unimpressive speed drops down to an even lower amount,
making him a very easy target for Snipers and Spies. His slow speed makes him more dependent on support from Medics and Engineers to keep him in the fight.
Aside from decimating entire teams, the Heavy is able to provide further support for his comrades with an often required health boost via his Sandvich, which, when consumed,
is capable of healing him to full health. It can also be dropped to provide an instant 50% health boost to his teammates, systematically equal to a Medium Health kit. However,
if the Heavy isnt careful, an enemy may pick up the dropped Sandvich for a health boost of their own.
The Heavy is the face of Team Fortress 2, and the character many players often think of when they hear of the game. 
He appears prominently on box-art, promotional materials, and loading screens. He starred in the very first Meet the Team video, and he has appeared in all further videos to date.
The Heavy is voiced by Gary Schwartz.',
 'https://i.redd.it/ss50w4h96uf41.png');
 
INSERT INTO tf2_db.class (id, charactername, characterdescription, imagesource)
VALUES (4, 'Spy',
 'Hailing from an indeterminate region of France, the Spy is an enthusiast of sharp suits and even sharper knives. Using a unique array of cloaking watches,
he can render himself invisible or even fake his own death, leaving unaware opponents off-guard. His Disguise Kit lets him take on the form of any class on either team,
allowing him to blend in while behind enemy lines before stabbing his unsuspecting "teammates" in the back. In fact, a swift backstab with any of the Spys knives will kill most foes in a single hit
provided they arent under the effects of any type of invulnerability, or some other form of immense damage reduction.
In addition to being able to swiftly assassinate key enemies, the Spy possesses the ability to disable and destroy Engineer-constructed buildings with his Sapper.
Once attached to an enemy building, the Sapper disables and slowly drains health from the building. However, a Sapper can be removed by an Engineer, or Pyro wielding the Homewrecker,
Maul, or Neon Annihilator.
The Spy, while disguised, has access to enemy Dispensers and Teleporters. This enables him to replenish his health and ammo behind enemy lines and navigate while blending in.
Despite being disguised, the Spy will still collide with enemy buildings.
Enemy Medics can both heal and apply ÜberCharge effects to disguised enemy Spies.
Whereas most players can only see the names and health of their teammates, the Spy can observe the names and health of the enemy team as well, allowing him to relay useful intelligence.
This ability is unique to only the Spy and a Medic carrying the Solemn Vow.
The Spy is voiced by Dennis Bateman.',
 'https://wiki.teamfortress.com/w/images/thumb/d/df/Spytaunt3.PNG/350px-Spytaunt3.PNG');
 
INSERT INTO tf2_db.class (id, charactername, characterdescription, imagesource)
VALUES (5, 'Medic',
 'The Medic is a Teutonic man of medicine from Stuttgart, Germany. While he may have a tenuous adherence to medical ethics,
he is nonetheless the primary healing class of the team. Although the Medics Syringe Gun and Bonesaw arent the most excellent weapons for direct combat,
he can typically still be found near the front lines, healing wounded teammates while trying to stay out of enemy fire.
When the Medic focuses his Medi Gun on a teammate, they will gradually regain health points.
Teammates who are already at full health will have their health buffed beyond the standard limit, going up to 150% of their base health capacity (with the exception of weapons that have overheal penalties,
such as the Fists of Steel and the Quick-Fix). Teammates who have not taken damage recently will be healed more rapidly, encouraging other players withdrawals when injured.
When healing, the Medic will gradually fill a unique ÜberCharge bar, which can fill faster if healing injured or not-fully-overhealed teammates.
When the ÜberCharge bar is fully charged, the Medics Medi Gun will begin to crackle, accompanied by small electric Team-colored particles at its tip,
indicating that he can now deploy a unique charge to benefit his healing target, and himself, for eight seconds. A charge from the Medi Gun offers temporary invulnerability;
a charge from the Kritzkrieg grants guaranteed critical hits; the Quick-Fix grants rapid healing,
along with immunity to knockback and compression blasts from Pyros; and the Vaccinator grants enhanced damage resistance to a specific damage type (bullets, explosives, and fire).
Although the Medic cannot heal himself, unless using an ÜberCharge-deployed Quick-Fix, or the Kritzkriegs Oktoberfest taunt,
he is capable of slowly regenerating health over time, and is the only class capable of doing this naturally. The longer a Medic stays out of combat and avoids taking damage,
the greater the increments of health he will passively heal. An injured Medic will begin regenerating health at a rate of 3 health points per second,
scaling up over the following ten seconds to a maximum of 6 health per second (this base rate can be positively affected by the Amputator and negatively affected by the Blutsauger).
The Medic is voiced by Robin Atkin Downes.',
 'https://wiki.teamfortress.com/w/images/thumb/2/26/Medic.png/250px-Medic.png');
 
INSERT INTO tf2_db.class (id, charactername, characterdescription, imagesource)
VALUES (6, 'Soldier',
 'The Soldier is a crazed, jingoistic patriot from Midwest, USA. Tough and well-armed, he is versatile, capable of both offense and defense, and a great starter class to get familiar with the game.
Well-balanced and possessing both survivability and mobility, the Soldier is considered one of the most flexible classes in Team Fortress 2. 
Despite his low ground movement speed, he is capable of using rocket jumps to reach his destination quickly.
 His large health pool is second only to that of the Heavy, and his wide array of armaments and weaponry allows him to bring whatever weapon or equipment is best suited to the situation at hand.
The Soldier is well known for his spectacular rocket jump. In defiance of all good sense and judgment,
 the Soldier can detonate a rocket at his feet and launch himself skyward at the cost of some health (which can be reduced by the Gunboats, or negated altogether by the Rocket Jumper).
 This ability allows the Soldier to navigate the battlefield by air,
 appear from unexpected places, and reach areas off-limits even to the Scouts double jump and Hype jumps. The Soldiers time airborne from a rocket jump can be increased with the B.A.S.E. Jumper.
The Soldier was voiced by the late Rick May. He was voiced by May and Nolan North in Expiration Date.',
 'https://wiki.teamfortress.com/w/images/thumb/7/7b/Soldier.png/250px-Soldier.png');
 
INSERT INTO tf2_db.class (id, charactername, characterdescription, imagesource)
VALUES (7, 'Demoman',
 'The Demoman is a scrumpy-swilling demolitions expert from the Scottish town of Ullapool, and is one of the most versatile members of the team.
 A master of explosives, the Demoman strategically deals massive amounts of indirect and mid-range splash damage. Armed with his Grenade Launcher and Stickybomb Launcher,
the Demoman uses his one good eye and the knowledge of his surrounding environment for well-timed sticky bomb detonations that send enemies skyward, often in many pieces.
Should anyone get past his explosive ordinance, however, they will be shocked to learn the Demoman is extremely proficient at melee combat, being one of the deadliest melee users in the game,
with a variety of powerful melee unlocks in his arsenal. The Demoman excels at swift destruction; he can bounce his grenades at creative angles to wreak havoc on enemy Sentry Gun emplacements while remaining safely out of sight.
His sticky bombs are a perfect tool for area denial, and are effective at keeping opponents away from any carts, control points, or Intelligence that he deems off-limits.
The Demoman is voiced by Gary Schwartz.',
 'https://wiki.teamfortress.com/w/images/thumb/f/fd/Demoman.png/250px-Demoman.png');
 
INSERT INTO tf2_db.class (id, charactername, characterdescription, imagesource)
VALUES (8, 'Sniper',
 'Hailing from the lost country of New Zealand and raised in the unforgiving Australian outback, the Sniper is a tough and ready crack shot. The Snipers main role on the battlefield is to pick off important enemy targets from afar using his Sniper Rifle and its ability to deal guaranteed critical hits with a headshot (with some exceptions). 
He is effective at long range, but weakens with proximity, where he is forced to use his Submachine Gun or his Kukri. As a result, 
the Sniper tends to perch on higher grounds or in hard-to-see places, where he can easily pin down enemies at chokepoints.
Although he is typically known for instantaneously killing enemies at a distance, the Sniper can use the Huntsman to get closer to the enemy.
Additionally, the Sydney Sleeper and the mysterious contents of Jarate allow him to take on a support role by causing enemies to take mini-crits.
The Sniper is voiced by John Patrick Lowrie.',
 'https://static.wikia.nocookie.net/teamfortress/images/8/8f/Sniper.png/revision/latest?cb=20170717231321');
 
INSERT INTO tf2_db.class (id, charactername, characterdescription, imagesource)
VALUES (9, 'Engineer',
 'The Engineer is a soft-spoken, amiable Texan from Bee Cave, Texas, USA with an interest in all mechanical things. 
He specializes in constructing and maintaining Buildings that provide support to his team, rather than fighting at the front lines,
making him the most suitable for defense. The Engineers various gadgets include the Sentry Gun, an automated turret that fires at any enemy in range,
the Dispenser, a device that restores the health and ammunition of nearby teammates, and Teleporters that quickly transport players from point A to point B.
Because the Engineers ingenious devices are under constant threat from explosives and devious enemy Spies,
a good Engineer must keep his gear under a watchful eye and under repair with his Wrench at all times. When the Engineer needs to get his hands dirty,
his trio of generic yet capable weapons, along with the assistance of his helpful hardware, make him more than capable of holding his own in a fight. If need be,
the Engineer can even pick up and haul constructed buildings to redeploy them in more favorable locations. While usually viewed as a defensive class,
the Engineer has a selection of high-tech weapons that allow him to destroy projectiles or to build faster-deploying, less damaging Sentry Guns,
which give him great utility at the front lines too. His Teleporters are also a key point to both the offensive and defensive teams success, allowing slower,
heavier classes to reach the front-lines quicker.
The Engineer is voiced by Grant Goodeve in the game, and by Nolan North in the short film Expiration Date.',
 'https://wiki.teamfortress.com/w/images/thumb/d/d8/Engineer.png/375px-Engineer.png');
>>>>>>> c6c2f4e6f55a4dbc860a316e452b8680d659b51d
